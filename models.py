from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from sqlalchemy.dialects.mysql import TIME
import os 

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False 

dbname = os.path.abspath(os.path.dirname(__file__))
dbase = os.path.join(dbname, 'db.sqlite')
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + dbase

db = SQLAlchemy(app)


class Ustadz(db.Model):
    __tablename__ = 'ustadz'
    id = db.Column(db.Integer, primary_key=True)
    nama = db.Column(db.String(90))
    foto = db.Column(db.TEXT)
    playlist = db.relationship('Playlist', backref='siapa_ustadznya', lazy=True)
    kajian = db.relationship('Kajian', backref='siapa_ustadznya')
    created_at = db.Column(db.DateTime, nullable=False,
                           default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, onupdate=datetime.utcnow)


class Kajian(db.Model):
    __tablename__ = 'kajian'
    id = db.Column(db.Integer, primary_key=True)
    judul= db.Column(db.String(90))
    thumbnail = db.Column(db.TEXT)
    ustadz_id = db.Column(db.Integer, db.ForeignKey('ustadz.id'), nullable=False)
    playlists = db.relationship('Playlist', backref='playlist_mana')
    desc = db.Column(db.TEXT)
    url_file = db.Column(db.String(255))
    file_type = db.Column(db.String(100))
    date = db.Column(db.Date)
    time = db.Column(TIME())
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, onupdate=datetime.utcnow)


class Playlist(db.Model):
    __tablename__ = 'playlist'
    id = db.Column(db.Integer, primary_key=True)
    nama_playlist = db.Column(db.String(100))
    kumpulan_kajian = db.Column(db.String(100), db.ForeignKey('kajian.id'))
    ustadz_id = db.Column(db.String(100), db.ForeignKey('ustadz.id'))



# class Person(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     name = db.Column(db.String(100))
#     pets = db.relationship('Pet', backref='owner')

# class Pet(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     name = db.Column(db.String(100))
#     owner_id = db.Column(db.Integer, db.ForeignKey('person.id'))

# db.create_all()